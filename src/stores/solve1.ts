import { ref } from "vue";
import { defineStore } from "pinia";

export default interface Menu {
  name: string;
  img: string;
}

export const useSolve1Store = defineStore("menu", () => {
  const currentSrc = ref(0);
  const active = ref(false);

  const img_src = [
    "feeling_imgs/smile.png",
    "feeling_imgs/cute.png",
    "feeling_imgs/excited.png",
    "feeling_imgs/pain.png",
    "feeling_imgs/shocked.png",
    "feeling_imgs/angry.png",
  ];

  return { currentSrc, img_src, active };
});
